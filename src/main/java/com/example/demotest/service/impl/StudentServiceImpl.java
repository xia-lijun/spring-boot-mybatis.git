package com.example.demotest.service.impl;

import com.example.demotest.service.StudentService;
import com.example.demotest.pojo.Student;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xiaoxia
 * @version 1.0
 */
@Service
//@SuppressWarnings("all")
public class StudentServiceImpl implements StudentService {
    private static List<Student> studentsAll = new ArrayList<>();
    static {
        studentsAll.add(new Student(1L, "夏礼俊" ,"敲代码"));
        studentsAll.add(new Student(2L, "小张" ,"打游戏"));
        studentsAll.add(new Student(3L, "张小杰" ,"游戏"));
    }

    @Override
    public List<Student> getAllStudents() {
        return studentsAll;
    }

    @Override
    public boolean insertOrUpdateStudent(Student stu) {
        if (stu.getId() == null) {
//            UUID uuid = UUID.randomUUID();
            Student student1 = new Student();
            student1.setId(1L);
            student1.setName(stu.getName());
            student1.setHobby(stu.getHobby());
            System.out.println(studentsAll);
            return studentsAll.add(student1);
        }else {
            studentsAll.stream().forEach(student -> {
                if (student.getId().equals(stu.getId())) {
                    student.setName(stu.getName());
                    student.setHobby(stu.getHobby());
                }
            });
            System.out.println("修改之后的数据是"+studentsAll);
        }
       return true;
    }

    @Override
    public Boolean deleteStudent(Long id) {
        List<Student> collect = studentsAll.stream().
                filter(student -> student.getId().equals(id)).collect(Collectors.toList());
//        studentsAll.stream().filter(new Predicate<Student>() {
//            @Override
//            public boolean test(Student student) {
//                return student.getId().equals(id);
//            }
//        });
        if (CollectionUtils.isEmpty(collect)){
            return false;
        }
        System.out.println(studentsAll);
        boolean remove = studentsAll.remove(collect.get(0));
        System.out.println(studentsAll);
        return remove;
    }
}
