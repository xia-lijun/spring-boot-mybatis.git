package com.example.demotest.service;

import com.example.demotest.pojo.Student;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaoxia
 * @version 1.0
 */
@Service
@SuppressWarnings({"all"})
public interface StudentService {
    //查询所有的学生
    List<Student> getAllStudents();
    //增加一个学生
    boolean insertOrUpdateStudent(Student stu);

    Boolean deleteStudent(Long id);
}
