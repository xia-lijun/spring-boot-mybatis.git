package com.example.demotest.service;

import com.example.demotest.config.Result;
import com.example.demotest.pojo.Person;
import org.springframework.stereotype.Service;

/**
 * @author xiaoxia
 * @version 1.0
 */
@Service
public interface PersonService {
    Result getAllPersons(Integer limit,Integer page);

    Boolean deleteOne(Long id);

    Boolean updateOrInsert(Person person);


    Result selectByTiaoJian(String name, String address, Integer limit, Integer page);
    Result getAllPersonsTest();
}
