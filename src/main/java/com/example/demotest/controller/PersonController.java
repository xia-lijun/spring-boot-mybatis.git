package com.example.demotest.controller;

import com.example.demotest.service.PersonService;
import com.example.demotest.config.Result;
import com.example.demotest.config.ResultCode;
import com.example.demotest.pojo.Person;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xiaoxia
 * @version 1.0
 */
@RestController
@RequestMapping("personTest")
@Slf4j
@SuppressWarnings({"all"})
public class PersonController {
    @Resource
    private PersonService personService;

    @ApiOperation(value = "测试查询所有人的数据")
    @GetMapping("/getAll")
    public Result getAllPersons(@RequestParam("limit")Integer limit ,
                                @RequestParam("page") Integer page){
        Result allPersons = personService.getAllPersons(limit, page);
        log.info("当前结果为:{}",allPersons.getData());
        return allPersons;
    }
    @ApiOperation(value = "删除一个人的数据")
    @GetMapping("/deleteOne")
    public Result deleteOne(@RequestParam Long id){
        Boolean trueOrFalse = personService.deleteOne(id);
        if (trueOrFalse) {
            return new Result(ResultCode.SUCCESS,trueOrFalse);
        }
       return new Result(ResultCode.ERROR, "删除信息失败");
    }
    @ApiOperation(value = "新增或者修改一个人的信息")
    @PostMapping("/updateOrInsert")
    public Result updateOrInsert(@RequestBody Person person){
        Boolean updateOrInsert = personService.updateOrInsert(person);
        if (updateOrInsert) {
            return new Result(ResultCode.SUCCESS,updateOrInsert);
        }
       return new Result(ResultCode.ERROR, "新增或者修改一个人的信息失败");
    }
    @ApiOperation(value = "条件查询带分页")
    @GetMapping("/selectByTiaoJian")
    public Result selectByTiaoJian(@RequestParam("name") String name,
                                   @RequestParam("address") String address,
                                   @RequestParam("limit")Integer limit ,
                                   @RequestParam("page") Integer page){
        return personService.selectByTiaoJian(name,address,limit,page);
    }
    @ApiOperation(value = "测试查询所有人的数据")
    @GetMapping("/getAllTest")
    public Result getAllPersonsTest(){
        Result allPersons = personService.getAllPersonsTest();
        log.info("当前结果为:{}",allPersons.getData());
        return allPersons;
    }
}
