package com.example.demotest.controller;

import com.example.demotest.service.StudentService;
import com.example.demotest.pojo.Student;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xiaoxia
 * @version 1.0
 */
@RestController
@RequestMapping("StudentDemo")
public class StudentController {
    @Resource
    private StudentService studentService;

    @ApiOperation("查询所有的学生信息")
    @PostMapping("/getAll")
    public List<Student> StudentSelect(){
        return studentService.getAllStudents();
    }
    @ApiOperation("增加或者修改一个学生的信息")
    @PostMapping("/InsertStudent")
    public Boolean StudentInsert(Student stu){
        return studentService.insertOrUpdateStudent(stu);
    }
    @ApiOperation("删除一个学生的信息")
    @PostMapping("/DeleteStudent")
    public Boolean DeleteStudent(Long id){
        return studentService.deleteStudent(id);
    }
}
