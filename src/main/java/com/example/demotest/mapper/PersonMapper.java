package com.example.demotest.mapper;

import com.example.demotest.pojo.Person;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xiaoxia
 * @version 1.0
 */
@Mapper
public interface PersonMapper {
    List<Person> getAllPersons();

    int deleteOne(Long id);

    int update(Person person);

    int insert(Person person1);

    List<Person> selectByTiaoJian(@Param("name") String name, @Param("address") String address);
}
