package com.example.demotest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author xiaoxia
 * @version 1.0
 */
@Configuration
@EnableWebMvc
@EnableOpenApi
public class SwaggerConfig {


}
