package com.example.demotest.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xiaoxia
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @ApiModelProperty("学生id")
    private Long id;
    @ApiModelProperty("学生姓名")
    private String name;
    @ApiModelProperty("学生兴趣")
    private String hobby;
}
