package com.example.demotest.utils;

/**
 * @author xiaoxia
 * @version 1.0
 */
public class SystemUtil {
    public static String getRandomNumberByNum(int num) {

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < num; i++) {

            long randomNum = Math.round(Math.floor(Math.random() * 10.0D));

            sb.append(randomNum);

        }

        return sb.toString();

    }
}
