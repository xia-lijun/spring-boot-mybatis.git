package com.example.demotest;

import com.example.demotest.config.Result;
import com.example.demotest.controller.PersonController;
import com.example.demotest.pojo.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
@Slf4j
class DemoTestApplicationTests {
    @Resource
    private PersonController personController;
//    @Test
//    void contextLoads() {
//        List<Person> allPersons =
//                personController.getAllPersons();
//        allPersons.forEach(System.out::println);
//    }
//    @Test
//    public void test1(){
//        Result aBoolean = personController.deleteOne(1L);
//        if (aBoolean) {
//            log.info("删除信息成功",aBoolean);
//        }else {
//            log.info("删除人员信息失败:{}" ,aBoolean);
//        }
//    }
//    @Test
//    public void test2() {
//        Person person = new Person();
//        person.setId(2L);
//        person.setName("张大山");
//        person.setZhiye("0");
//        Boolean aBoolean = personController.updateOrInsert(person);
//        if (aBoolean) {
//            if (person.getId() != null) {
//                log.info("修改信息成功");
//            }else {
//                log.info("新增信息成功");
//            }
//        }else {
//            log.info("失败");
//        }
//    }
    @Test
    public void test3(){
        Person person = new Person();
        person.setName("夏");
        person.setAddress("上海");
        int limit = 10;
        int page = 1;
        Result result = personController.selectByTiaoJian(person.getName(), person.getAddress(), limit, page);
        log.info("当前的查询的分页模糊查询的结果为:{}",result.getData());
    }
}
